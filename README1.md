# Weekly Assesment 4

# Logic Test

Please make javascript file to submit this

## 1. Area Of Triangle

Write a function that takes the base and height of a triangle and `return` its area.

### Examples

```
triArea(3, 2) ➞ 3

triArea(7, 4) ➞ 14

triArea(10, 10) ➞ 50
```

### Notes

-   The area of a triangle is: `(base * height) / 2`
-   Don't forget to `return` the result.


##  2. Convert Hours into Second

Write a function that converts `hours` into seconds.

### Examples

```
howManySeconds(2) ➞ 7200

howManySeconds(10) ➞ 36000

howManySeconds(24) ➞ 86400
```

### Notes

-   60 seconds in a minute, 60 minutes in an hour
-   Don't forget to `return` your answer.


## GIT Question

Submit with md extension file

- How the workflow for git until it's updated to repository ?
- Whats git? What's git use for?
- How to track git progress ?


# IMPORTANT

Please Submit to this form:
https://docs.google.com/forms/d/e/1FAIpQLSfQRp5hy12md1Y9AWTtYV6a76D3Nh74coQUbnJ6fSApXAeV_Q/viewform
